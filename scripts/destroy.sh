#!/bin/bash
aws cloudformation delete-stack \
    --stack-name "queue-medic-test-stack"

aws cloudformation delete-stack \
    --stack-name "queue-medic"
