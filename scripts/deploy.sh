#!/bin/bash

# Test stack
pipenv lock -r >> tests/stack/requirements.txt

sam package \
    --template-file tests/stack/template.yml \
    --s3-bucket "${S3_BUCKET_NAME}" \
    --s3-prefix "queue-medic/${S3_PREFIX}/test-stack" \
    --output-template-file tests/stack/packaged-template.yml

sam deploy \
    --template-file tests/stack/packaged-template.yml \
    --stack-name "queue-medic-test-stack" \
    --capabilities CAPABILITY_IAM

aws s3 rm "s3://${S3_BUCKET_NAME}/queue-medic/${S3_PREFIX}/test-stack" --recursive

rm tests/stack/packaged-template.yml
rm tests/stack/requirements.txt

# Main stack
pipenv lock -r >> tests/stack/requirements.txt

sam package \
    --template-file template.yml \
    --s3-bucket "${S3_BUCKET_NAME}" \
    --s3-prefix "queue-medic/${S3_PREFIX}" \
    --output-template-file packaged-template.yml

sam deploy \
    --template-file packaged-template.yml \
    --stack-name "queue-medic" \
    --capabilities CAPABILITY_IAM

aws s3 rm "s3://${S3_BUCKET_NAME}/queue-medic/${S3_PREFIX}" --recursive

rm packaged-template.yml
rm requirements.txt
