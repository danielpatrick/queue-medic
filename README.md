# Queue Medic

Proof of concept of how to handle failure for multiple AWS SQSes with their respective dead letter queues. The goal is to be able to apply simple fixes and redrive queue events back to their original SQS using a simple UI... eventually.


Developed and tested with Python 3.7.


## Development

To get started, first install pipenv:

`sudo pip3 install pipenv`

Then you can use the `make` commands:

* `make local` - install the local dependencies and create a virtual environment
* `make lint` - run linter (flake8)
* `make test` - run tests using pytest


## Deploying to AWS

To deploy to AWS you will need `aws-cli` and `aws-sam-cli` and will need to:
- Set your AWS credentials as per the `aws-cli` documentation.
- Create an S3 bucket for staging deployment code.
- Copy `.env.sample` to `.env` and set the environment variables.

You can then run:
* `make deploy` - deploy stack to AWS
* `make destroy` - delete stack from AWS

## Architecture

Very much a work in progress, but plan is for something like:
![Architecture diagram](architecture/Queue%20Medic.png)

There's a [more amibtious plan](architecture/Queue%20Medic%20Ambitious.png) as well.
