.PHONY: local lint

-include .env
PYTHONPATH := ${PYTHONPATH}:${shell pwd}/src
export

TEST_STACK_NAME=queue-medic-test-stack
STACK_NAME=queue-medic

.ONESHELL:
local:
	find .git/hooks -type l -exec rm {} \;
	find .githooks -type f -exec chmod +x '{}' \;
	find .githooks -type f -exec ln -sf ../../{} .git/hooks/ \;
	pipenv install --dev

.ONESHELL:
lint:
	pipenv run flake8

.ONESHELL:
cfn_lint:
	pipenv run cfn-lint template.yaml

.ONESHELL:
test:
	echo ${PYTHONPATH}
	pipenv run pytest tests

.ONESHELL:
build:
	# Test stack
	pipenv lock -r >> tests/stack/requirements.txt

	sam build --use-container \
		-t tests/stack/template.yaml \
		-m tests/stack/requirements.txt \
		-b dist/test

	rm tests/stack/requirements.txt

	# Main stack
	pipenv lock -r >> requirements.txt

	sam build --use-container \
		-t template.yaml \
		-m requirements.txt \
		-b dist/main

	rm requirements.txt

.ONESHELL:
deploy:
	# Test stack
	sam package \
		--template-file dist/test/template.yaml \
		--s3-bucket "${S3_BUCKET_NAME}" \
		--s3-prefix "${TEST_STACK_NAME}/${S3_PREFIX}" \
		--output-template-file dist/test/packaged.yaml

	sam deploy \
		--template-file dist/test/packaged.yaml \
		--stack-name ${TEST_STACK_NAME} \
		--capabilities CAPABILITY_IAM

	aws s3 rm "s3://${S3_BUCKET_NAME}/${TEST_STACK_NAME}/${S3_PREFIX}" \
		--recursive

	# Main stack
	sam package \
		--template-file dist/main/template.yaml \
		--s3-bucket "${S3_BUCKET_NAME}" \
		--s3-prefix "${STACK_NAME}/${S3_PREFIX}" \
		--output-template-file dist/main/packaged.yaml

	sam deploy \
		--template-file dist/main/packaged.yaml \
		--stack-name ${STACK_NAME} \
		--capabilities CAPABILITY_IAM

	aws s3 rm "s3://${S3_BUCKET_NAME}/${STACK_NAME}/${S3_PREFIX}" --recursive

.ONESHELL:
destroy:
	aws cloudformation delete-stack \
		--stack-name ${STACK_NAME}

	echo "DELETING STACK ${STACK_NAME}"

	aws cloudformation wait stack-delete-complete \
		--stack-name ${STACK_NAME}

	echo "STACK DELETED ${STACK_NAME}"

	aws cloudformation delete-stack \
		--stack-name ${TEST_STACK_NAME}

	echo "DELETING STACK ${TEST_STACK_NAME}"

	aws cloudformation wait stack-delete-complete \
		--stack-name ${TEST_STACK_NAME}

	echo "STACK DELETED ${TEST_STACK_NAME}"
